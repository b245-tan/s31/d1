// Use the "require" directive to load Node.js modules.
// A "module" is a software component or part of a program that contains one or more routines.
// "http module" lets Node.js transfer data using the hyper text transfer protocol.

let http = require("http");

http.createServer(function(req, res) {

	// Use the writehead() method to:
	// Set a status code for the response, 200 means 'OK'
	// Set the content-type of the response as a plain text message
	res.writeHead(200, {'Content-Type': 'text/plain'});
	res.end("Hello, B245!");

}).listen(4000);

// When server is running, console will print the message;
console.log('Server running at localhost:4000');